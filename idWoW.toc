## Interface: 30300
## Title: idWoW
## Version: 1.9
## Notes: Adds an ID for spells, items, symbols, units to a tooltip.
## Notes-ruRU: Добавляет ID для заклинаний, предметов, символов, юнитов во всплывающую подсказку.
## Author: Artur91425

core.lua
