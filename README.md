## Installation
1. Download **[Latest Version](https://gitlab.com/Artur91425/idWoW/-/archive/master/idWoW-master.zip)**
2. Unpack the Zip file
3. Rename the folder "idWoW-master" to "idWoW"
4. Copy "idWoW" into Wow-Directory\Interface\AddOns
5. Restart Wow

## Screenshots
<img src="https://user-images.githubusercontent.com/24303693/63615202-70afc800-c5ed-11e9-95f3-f1ba8704ecfe.jpg">
<img src="https://user-images.githubusercontent.com/24303693/63615205-71485e80-c5ed-11e9-9090-36343ec396c7.jpg">
<img src="https://user-images.githubusercontent.com/24303693/63615204-70afc800-c5ed-11e9-9b02-74edf37fb0b0.jpg">
<img src="https://user-images.githubusercontent.com/24303693/63615206-71485e80-c5ed-11e9-854a-eaccc1bfd0d7.jpg">
